package eg.edu.alexu.csd.oop.game;

import java.util.logging.Logger;

import eg.edu.alexu.csd.oop.game.view.COPFrame;
import eg.edu.alexu.csd.oop.game.view.StartFrame;

@SuppressWarnings("unused")
public class COP {

	public static double RATIO_TO_1080 = 1080.0 / COPFrame.getScreenSize().getHeight();
	//public final static Logger logr = LoggingManger.setupLogger(); // <- watch this first https://youtu.be/W0_Man88Z3Q

	public static void main(String[] args) throws Exception {

		StartFrame.getInstance().showFrame();

	}

}
