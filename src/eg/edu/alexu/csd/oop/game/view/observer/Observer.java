package eg.edu.alexu.csd.oop.game.view.observer;

//The Observers update method is called when the Subject changes

public interface Observer {

	public void update(int value);

}