package eg.edu.alexu.csd.oop.game.view;

import java.awt.Dimension;
import java.awt.Image;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import java.io.IOException;
import java.net.URL;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

/**
 * @dPattern Singleton
 */
@SuppressWarnings("serial")
public class GameOverFrame extends COPFrame {

	private static GameOverFrame single_instance = null;

	private GameOverFrame() {
		pointerToMySelf = this;
		initiateFrame();
		layoutButtons();
	}

	public static GameOverFrame getInstance() {
		if (single_instance == null)
			single_instance = new GameOverFrame();

		return single_instance;
	}

	Clip endClip;

	public void showFrame() {

		COPFrame.soundClip.stop();
		endClip.setFramePosition(0);
		endClip.loop(Clip.LOOP_CONTINUOUSLY);

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setResizable(false);
		setVisible(true);
	}

	protected void initiateFrame() {
		setTitle("GameOver");
		ImageIcon image = new ImageIcon(COPFrame.getRelativePakagePath("icon.png"));
		setIconImage(image.getImage());
		File soundtrack = new File(getRelativePakagePath("gameovertrack.wav"));
		URL url;
		try {
			url = new URL(soundtrack.toURI().toURL(), "gameovertrack.wav");
			endClip = AudioSystem.getClip();
			AudioInputStream ais = AudioSystem.getAudioInputStream(url);
			endClip.open(ais);
		} catch (UnsupportedAudioFileException | IOException | LineUnavailableException e) {
			e.printStackTrace();
		}
		removeBackgroundColor();
		setUpBackground();
	}

	private void setUpBackground() {
		Icon background = new ImageIcon(COPFrame.getRelativePakagePath("gameover.gif"));
		setSize((int) (StartFrame.getScreenSize().width / 1.5), (int) (StartFrame.getScreenSize().height / 1.5));
		setLocationRelativeTo(null);
		setContentPane(new JLabel(background));
	}

	protected void layoutButtons() {
		JLabel backbtn = new JLabel("");
		backbtn.setIcon(new ImageIcon(new ImageIcon(COPFrame.getRelativePakagePath("Settingback.png")).getImage()
				.getScaledInstance((int) (182 / 3),(int) (191 / 3), Image.SCALE_DEFAULT)));
		Dimension size = backbtn.getPreferredSize();
		backbtn.setBounds((int) (520/FIXED_RATIO_TO_1080), (int) (470/FIXED_RATIO_TO_1080), size.width, size.height);
		backbtn.addMouseListener((MouseListener) new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				dispose();
				endClip.stop();
				StartFrame.getInstance().showFrame();
				COPFrame.soundClip.loop(Clip.LOOP_CONTINUOUSLY);
				COPFrame.soundClip.setFramePosition(0);
			}

		});
		add(backbtn);

		JLabel exitbtn = new JLabel("");
		exitbtn.setIcon(new ImageIcon(new ImageIcon(COPFrame.getRelativePakagePath("exitover.png")).getImage()
				.getScaledInstance((int) (182 / 3),(int) (191 / 3), Image.SCALE_DEFAULT)));
		Dimension size2 = exitbtn.getPreferredSize();
		exitbtn.setBounds((int) (650/FIXED_RATIO_TO_1080), (int) (470/FIXED_RATIO_TO_1080), size2.width, size2.height);
		exitbtn.addMouseListener((MouseListener) new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				if (JOptionPane.showConfirmDialog(pointerToMySelf, "Confirm if you Want to Exit", "Exit!",
						JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION)
					System.exit(0);
			}

		});
		add(exitbtn);
	}

	public void close() {
		dispose();
	}

}
