package eg.edu.alexu.csd.oop.game.view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.sound.sampled.Clip;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.Timer;

import eg.edu.alexu.csd.oop.game.COPGameEngine;
import eg.edu.alexu.csd.oop.game.GameEngine.GameController;
import eg.edu.alexu.csd.oop.game.model.world.AbstractWorldModel;
import eg.edu.alexu.csd.oop.game.model.world.COPWorldModel;

@SuppressWarnings({ "serial" })
public class GamePanel extends JPanel {

	public static boolean gameOver = false;

	static public int soundclicked = 1;

	private Dimension scoreboardsize;
	private static JFrame containerWindow;

	static GameController gameController;
	static Timer timeCountDown;

	private int score = 0;
	private int timeRemaining = 120;

	public GamePanel() {
		
		gameOver = false;

		constractPanel();
		
		Image gif = sendGIF();

		containerWindow = new JFrame();

		JFrame[] subFrames = { PausedFrame.getInstance() };

		scoreboardsize = new Dimension();

		AbstractWorldModel world = new COPWorldModel(SettingsFrame.GameWindowSize.width,
				SettingsFrame.GameWindowSize.height, LevelsFrame.level);

		world.addScoreUpdatingListener(new ScoreUpdatingListener() {
			public void scoreUpdated(int newScore) {
				score = newScore;
			}
		});

		gameController = COPGameEngine.start(containerWindow, this, gif, world);

		containerWindow.addWindowListener(new WindowListener() {

			public void windowOpened(WindowEvent e) {
			}

			public void windowIconified(WindowEvent e) {
			}

			public void windowDeiconified(WindowEvent e) {
			}

			public void windowDeactivated(WindowEvent e) {
			}

			public void windowClosing(WindowEvent e) {
			}

			public void windowClosed(WindowEvent e) {
			}

			public void windowActivated(WindowEvent e) {
				for (JFrame window : subFrames)
					if (window.isVisible())
						window.dispose();
				GamePanel.gameController.resume();
			}

		});
	}

	private Image sendGIF() {
		return Toolkit.getDefaultToolkit().createImage(StartFrame.getRelativePakagePath("game.gif")).getScaledInstance(
				SettingsFrame.GameWindowSize.width, SettingsFrame.GameWindowSize.height, Image.SCALE_DEFAULT);
	}

	private void constractPanel() {
		setLayout(null);
		setSize(SettingsFrame.GameWindowSize);
		putscore();
		puttime();
		putLevel();
		putHomeButton();
		putPauseButton();
		putsoundbutton();
	}

	private void putscore() {
		// ************************** Score Text*****************************//
		String CurrentScore = new String("00");
		JLabel Score = new JLabel(CurrentScore);
		Score.setForeground(Color.WHITE);
		Score.setFont(new java.awt.Font("Arial", java.awt.Font.PLAIN, 30));
		Dimension scoretext = Score.getPreferredSize();
		Score.setBounds(138, 7, scoretext.width, scoretext.height);

		Timer scoreTimer = new Timer(10, new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Score.setText(String.valueOf(score)); // get score from its function, VERY BAD METHOD
				Score.repaint();
			}
		});
		scoreTimer.start();
		add(Score);
		// ************************** Score Board *****************************//
		JLabel ScoreBoard = new JLabel("");
		ScoreBoard.setIcon(new ImageIcon(new ImageIcon(StartFrame.getRelativePakagePath("score.png")).getImage()
				.getScaledInstance(395 / 2, 56 / 2, Image.SCALE_DEFAULT)));
		scoreboardsize = ScoreBoard.getPreferredSize();
		ScoreBoard.setBounds(10, 10, scoreboardsize.width, scoreboardsize.height);
		add(ScoreBoard);
	}

	private void puttime() {
		/************************** TimeText *****************************/
		String Currenttime = new String("120");
		JLabel time = new JLabel(Currenttime);
		time.setForeground(Color.WHITE);
		time.setFont(new java.awt.Font("Arial", java.awt.Font.PLAIN, 30));
		Dimension timetext = time.getPreferredSize();
		time.setBounds(scoreboardsize.width + 138, 7, timetext.width, timetext.height);

		timeCountDown = new Timer(1000, new ActionListener() {

			public void actionPerformed(ActionEvent a) {
				if (timeRemaining <= 1) {
					gameOver = true;
					timeCountDown.stop();
				}
				timeRemaining--;
				time.setText(Integer.toString(timeRemaining));
				time.repaint();
			}

		});

		timeCountDown.start();
		add(time);
		// ************************** Time Board *****************************//
		JLabel timeBoard = new JLabel("");
		timeBoard.setIcon(new ImageIcon(new ImageIcon(StartFrame.getRelativePakagePath("timer.png")).getImage()
				.getScaledInstance(395 / 2, 56 / 2, Image.SCALE_DEFAULT)));
		Dimension timersize = timeBoard.getPreferredSize();
		timeBoard.setBounds(scoreboardsize.width + 20, 10, timersize.width, timersize.height);
		add(timeBoard);
	}

	private void putLevel() {
		// ************************** Level Text *****************************//
		JLabel mode = new JLabel(LevelsFrame.level.toString());
		mode.setForeground(Color.WHITE);
		mode.setFont(new java.awt.Font("Arial", java.awt.Font.PLAIN, 25));
		Dimension modetext = mode.getPreferredSize();
		mode.setBounds(103, scoreboardsize.height + 25, modetext.width, modetext.height);
		add(mode);
		// ************************** Score Board ****************************//
		JLabel modeBoard = new JLabel("");
		modeBoard.setIcon(new ImageIcon(new ImageIcon(StartFrame.getRelativePakagePath("level.png")).getImage()
				.getScaledInstance(395 / 2, 56 / 2, Image.SCALE_DEFAULT)));
		Dimension modeBoardsize = modeBoard.getPreferredSize();
		modeBoard.setBounds(10, scoreboardsize.height + 25, modeBoardsize.width, modeBoardsize.height);
		add(modeBoard);
	}

	private void putHomeButton() {

		JLabel homeButton = new JLabel("");
		homeButton.setIcon(new ImageIcon(new ImageIcon(StartFrame.getRelativePakagePath("home.png")).getImage()
				.getScaledInstance(182 / 3, 191 / 3, Image.SCALE_DEFAULT)));
		Dimension homeButtonSize = homeButton.getPreferredSize();
		homeButton.setBounds(SettingsFrame.GameWindowSize.width - homeButtonSize.width - 10, 10, homeButtonSize.width,
				homeButtonSize.height);
		homeButton.addMouseListener((MouseListener) new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				if (JOptionPane.showConfirmDialog(containerWindow, "Really want to exit?", "Exit!",
						JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
					closeGame();
					StartFrame.getInstance().showFrame();
				}
			}
		});

		add(homeButton);
	}

	private void putPauseButton() {

		JLabel pauseButton = new JLabel("");
		pauseButton.setIcon(new ImageIcon(new ImageIcon(StartFrame.getRelativePakagePath("pause.png")).getImage()
				.getScaledInstance(181 / 3, 263 / 3 - 10, Image.SCALE_DEFAULT)));
		Dimension pauseButtonSize = pauseButton.getPreferredSize();
		pauseButton.setBounds(SettingsFrame.GameWindowSize.width - pauseButtonSize.width * 2 - 20, 3,
				pauseButtonSize.width, pauseButtonSize.height);
		pauseButton.addMouseListener((MouseListener) new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				timeCountDown.stop();
				gameController.pause();
				PausedFrame.getInstance().showFrame();
			}
		});
		add(pauseButton);
	}

	private void putsoundbutton() {

		JLabel soundbutton = new JLabel("");
		if (soundclicked % 2 == 0) {

			soundbutton.setIcon(new ImageIcon(new ImageIcon(StartFrame.getRelativePakagePath("unmute.png")).getImage()
					.getScaledInstance(283 / 3, 232 / 3, Image.SCALE_DEFAULT)));

		} else {

			soundbutton.setIcon(new ImageIcon(new ImageIcon(StartFrame.getRelativePakagePath("mute.png")).getImage()
					.getScaledInstance(283 / 3, 232 / 3, Image.SCALE_DEFAULT)));
		}
		soundbutton.setIcon(new ImageIcon(new ImageIcon(StartFrame.getRelativePakagePath("mute.png")).getImage()
				.getScaledInstance(283 / 3, 232 / 3, Image.SCALE_DEFAULT)));
		Dimension soundbuttonsize = soundbutton.getPreferredSize();
		soundbutton.setBounds(SettingsFrame.GameWindowSize.width - soundbuttonsize.width * 2 - 40, 0,
				soundbuttonsize.width, soundbuttonsize.height);
		soundbutton.addMouseListener((MouseListener) new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				soundclicked++;
				if (soundclicked % 2 == 0) {
					COPFrame.soundClip.stop();
					soundbutton.setIcon(new ImageIcon(new ImageIcon(StartFrame.getRelativePakagePath("unmute.png"))
							.getImage().getScaledInstance(283 / 3, 232 / 3, Image.SCALE_DEFAULT)));

				} else {
					COPFrame.soundClip.loop(Clip.LOOP_CONTINUOUSLY);
					soundbutton.setIcon(new ImageIcon(new ImageIcon(StartFrame.getRelativePakagePath("mute.png"))
							.getImage().getScaledInstance(283 / 3, 232 / 3, Image.SCALE_DEFAULT)));
				}
			}
		});
		add(soundbutton);
	}

	public static void closeGame() {
		containerWindow.setVisible(false);
		containerWindow.dispose();
		gameController.pause();
	}

}
