package eg.edu.alexu.csd.oop.game.view;

public interface ScoreUpdatingListener {
	void scoreUpdated(int newScore);
}
