package eg.edu.alexu.csd.oop.game.view.observer;

//This interface handles adding, deleting and updating
//all observers 

public interface Subject {

	public void register(Observer o);

	public void notifyObservers(int value);

}