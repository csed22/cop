package eg.edu.alexu.csd.oop.game.controller.gameLoop;

public interface ScoreModificationListener {
	void scoreModificationHappened(int scoreModification);
}
