package eg.edu.alexu.csd.oop.game.controller.gameLoop;

import java.util.List;

import eg.edu.alexu.csd.oop.game.GameObject;
import eg.edu.alexu.csd.oop.game.model.world.AbstractWorldModel;

public abstract class AbstractGameLoop {

	protected final int MAX_TIME;
	protected final long startTime;
	protected final int width;
	protected final int height;

	/** dPattern Iterator */
	protected final List<GameObject> constant;
	/** dPattern Iterator */
	protected final List<GameObject> moving;
	/** dPattern Iterator */
	protected final List<GameObject> control;

	public AbstractGameLoop(AbstractWorldModel world) {
		this.startTime = world.startTime;
		this.MAX_TIME = world.MAX_TIME;
		this.width = world.getWidth();
		this.height = world.getHeight();
		this.constant = world.constant;
		this.moving = world.moving;
		this.control = world.control;
	}

	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}

	/**
	 * Game Loop logic refresh the world state and update locations
	 * 
	 * @author Abdelrahman, Zidan
	 * @dPattern Facade
	 * @return false means game over
	 */
	public abstract boolean updateAndCheck();

}
