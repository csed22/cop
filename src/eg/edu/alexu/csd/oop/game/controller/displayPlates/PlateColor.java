package eg.edu.alexu.csd.oop.game.controller.displayPlates;

public enum PlateColor {

	WHITE("white"), BLUE("blue"), GREEN("green"), ORANGE("orange"), RED("red"), YELLOW("yellow");

	private String name;

	PlateColor(String name) {
		this.name = name;
	}

	public String getName() {
		return this.name;
	}
}
