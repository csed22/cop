package eg.edu.alexu.csd.oop.game.controller.factory;

import eg.edu.alexu.csd.oop.game.controller.displayPlates.PlateColor;
import eg.edu.alexu.csd.oop.game.controller.displayPlates.PlateShape;
import eg.edu.alexu.csd.oop.game.controller.worldConstructor.Level;
import eg.edu.alexu.csd.oop.game.model.object.*;
import eg.edu.alexu.csd.oop.game.view.COPFrame;

import java.util.ArrayList;
import java.util.Random;

import static eg.edu.alexu.csd.oop.game.model.object.SizeConstant.W;

public class PlatesFactory implements AbstractObjectFactory {
	private ArrayList<COPPlateObject> plates = new ArrayList<>();
	private Level level;
	private Phase phase;
	private int worldWidth = COPFrame.getScreenSize().width;

	public PlatesFactory() {
		this.level = Level.EASY;
		this.phase = new Phase();
	}

	public void setWorldWidth(int worldWidth) {
		this.worldWidth = worldWidth;
	}

	public void setLevel(Level level) {
		this.level = level;
		phase = new Phase();
	}

	public COPPlateObject get(int x, int y) {
		if (stored() < 10)
			create();
		COPPlateObject plate;
		if (phase.isSelectedTime()) {
			plate = new COPPlateObject(x, y, random(PlateShape.class), phase.selectedColor);
		} else {
			plate = plates.remove(random());
			plate.setX(x);
			plate.setY(y);
		}
		if (plate.getX() > worldWidth - plate.cons(W)) {
			plate.setX((int) (plate.getX() - plate.cons(W)));
		}
		plate.setMotionType(COPPlateObject.PlateMotionType.REGULAR);
		plate.setVisible(true);
		phase.step();
		return plate;
	}

	public COPPlateObject get(int x, int y, double timestamp) {
		COPPlateObject plate = get(x, y);
		plate.setTimestamp(timestamp);
		return plate;
	}

	public COPPlateObject get(int x, int y, double timestamp, COPPlateObject.PlateMotionType motionType) {
		COPPlateObject plate = get(x, y, timestamp);
		plate.setMotionType(motionType);
		return plate;
	}

	public void collect(AbstractImageObject plate) {
		plates.add((COPPlateObject) plate);
		clean();
	}

	public void clean() {
		if (plates.size() < 50)
			return;
		Random random = new Random();
		for (int i = 0; i < 5; i++) {
			plates.remove(random.nextInt(plates.size()));
		}
	}

	public void create() {
		plates.add(new COPPlateObject(0, 0, random(PlateShape.class), random(PlateColor.class)));
	}

	public void fill() {
		for (int i = 0; i < 10; i++)
			create();
	}

	public int random() {
		return new Random().nextInt(plates.size());
	}

	public int stored() {
		return plates.size();
	}

	public static <T> T random(Class<T> enumClass) {
		Random random = new Random();
		int index = random.nextInt(enumClass.getEnumConstants().length);
		return enumClass.getEnumConstants()[index];
	}

	private class Phase {
		int duration;
		PlateColor selectedColor;
		final int NUM_SELECTED_IN_DURATION = 3;
		ArrayList<Integer> selectedTimes = new ArrayList<>();
		int turn = 0;

		Phase() {
			switch (level) {
			case EASY:
				duration = 10;
				break;
			case MEDIUM:
				duration = 15;
				break;
			case HARD:
				duration = 20;
				break;
			default:
				break;
			}
			select();
		}

		void select() {
			for (int i = 0; i < NUM_SELECTED_IN_DURATION; i++) {
				Random random = new Random();
				int time;
				while (selectedTimes.contains(time = random.nextInt(duration)))
					;
				selectedTimes.add(time);
			}
			selectedColor = random(PlateColor.class);
		}

		void step() {
			if (++turn >= duration) {
				selectedTimes.clear();
				select();
				turn = 0;
			}
		}

		boolean isSelectedTime() {
			if (selectedTimes.contains(turn))
				return true;
			return false;
		}
	}
}
