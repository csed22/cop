package eg.edu.alexu.csd.oop.game.controller.worldConstructor;

public enum Level {
    EASY,
    MEDIUM,
    HARD;

    public String toString() {
        switch (this){
            case MEDIUM: return "Medium";
            case HARD: return "Hard";
            default: return "Easy";
        }
    }
}
