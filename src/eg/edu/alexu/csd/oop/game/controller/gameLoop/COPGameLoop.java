package eg.edu.alexu.csd.oop.game.controller.gameLoop;

import static eg.edu.alexu.csd.oop.game.model.object.SizeConstant.A;
import static eg.edu.alexu.csd.oop.game.model.object.SizeConstant.B;
import static eg.edu.alexu.csd.oop.game.model.object.SizeConstant.C;
import static eg.edu.alexu.csd.oop.game.model.object.SizeConstant.D;
import static eg.edu.alexu.csd.oop.game.model.object.SizeConstant.E;
import static eg.edu.alexu.csd.oop.game.model.object.SizeConstant.F;
import static eg.edu.alexu.csd.oop.game.model.object.SizeConstant.H;
import static eg.edu.alexu.csd.oop.game.model.object.SizeConstant.W;

import java.awt.image.BufferedImage;
import java.util.ArrayList;

import eg.edu.alexu.csd.oop.game.GameObject;
import eg.edu.alexu.csd.oop.game.controller.displayPlates.PlateColor;
import eg.edu.alexu.csd.oop.game.controller.factory.PlatesFactory;
import eg.edu.alexu.csd.oop.game.controller.worldConstructor.Level;
import eg.edu.alexu.csd.oop.game.model.object.COPClownObject;
import eg.edu.alexu.csd.oop.game.model.object.COPCrashBurst;
import eg.edu.alexu.csd.oop.game.model.object.COPPlateObject;
import eg.edu.alexu.csd.oop.game.model.world.AbstractWorldModel;
import eg.edu.alexu.csd.oop.game.model.world.COPWorldModel;
import eg.edu.alexu.csd.oop.game.view.GameOverFrame;
import eg.edu.alexu.csd.oop.game.view.GamePanel;

/**
 * @dPatten Facade
 */
public class COPGameLoop extends AbstractGameLoop {

	public final Level LEVEL;
	public final PlatesFactory PLATES_FACTORY;
	private final COPClownObject CLOWN;
	private final double GRAVITY;
	private double time = 0;
	@SuppressWarnings("unused")
	private int scene = 0;
	private int totalCrashed = 0;
	private int continuouslyCrashed = 0;
	public final double T_g = 10.0 * 0.001; // game speed in seconds
	public final double D_c = 10.0; // control displacement in pixels
	public final double T_c = 0.036; // control control speed in seconds
	public final int D_i = 0; // initial plate displacement in pixels
	public final double V_i = 100; // initial velocity
	private final int TOTAL_CRASHED_LIMIT;
	private final int CONT_CRASHED_LIMIT;
	@SuppressWarnings("unused")
	private final double GRAVITY_LEVEL_FACTOR;
	private final double RANDOMNESS_LEVEL_FACTOR;
	private final double BAZOOKA_V_RATIO = 2;
	private ScoreModificationListener scoreListener;

	// do nothing in the constructor, unless you want more info to be passed in
	public COPGameLoop(AbstractWorldModel world) {
		super(world);
		this.LEVEL = ((COPWorldModel) world).LEVEL;
		this.PLATES_FACTORY = ((COPWorldModel) world).PLATES_FACTORY;
		this.CLOWN = (COPClownObject) control.get(0);
		switch (LEVEL) {
		case EASY:
			TOTAL_CRASHED_LIMIT = 9999;
			CONT_CRASHED_LIMIT = 9999;
			GRAVITY_LEVEL_FACTOR = 1.0;
			RANDOMNESS_LEVEL_FACTOR = 0.992;
			break;
		case MEDIUM:
			TOTAL_CRASHED_LIMIT = 9999;
			CONT_CRASHED_LIMIT = 9999;
			GRAVITY_LEVEL_FACTOR = 1.5;
			RANDOMNESS_LEVEL_FACTOR = 0.985;
			break;
		default:
			TOTAL_CRASHED_LIMIT = 9999;
			CONT_CRASHED_LIMIT = 9999;
			GRAVITY_LEVEL_FACTOR = 3.5;
			RANDOMNESS_LEVEL_FACTOR = 0.980;
		}
		final double T = T_c * (getWidth() - CLOWN.cons(W)) / D_c;
		GRAVITY = 2 * (getHeight() - (CLOWN.cons(H) - CLOWN.cons(E)) - D_i - V_i * T) / Math.pow(T, 2.0);// *
																											// GRAVITY_LEVEL_FACTOR;

	}

	private void incScore() {
		if (scoreListener != null)
			scoreListener.scoreModificationHappened(1);
	}

	private void decScore() {
		if (scoreListener != null)
			scoreListener.scoreModificationHappened(-1);
	}

	private void modScore(int modification) {
		if (scoreListener != null)
			scoreListener.scoreModificationHappened(modification);
	}

	public void setScoreListener(ScoreModificationListener scoreListener) {
		this.scoreListener = scoreListener;
	}

	public boolean updateAndCheck() {
		@SuppressWarnings("unused")
		boolean timeout = System.currentTimeMillis() - startTime > MAX_TIME; // time end and game over

		if (Math.random() > RANDOMNESS_LEVEL_FACTOR)
			moving.add(PLATES_FACTORY.get((int) (Math.random() * getWidth()), D_i, time));
		if (Math.random() > 0.9998) {
			COPPlateObject plate = PLATES_FACTORY.get((int) (Math.random() * getWidth()), D_i, time,
					COPPlateObject.PlateMotionType.BAZOOKA);
			moving.add(plate);
			COPCrashBurst burst = new COPCrashBurst(
					plate.getX() - Math.abs((COPCrashBurst.getSize(getWidth(), W) - plate.getWidth()) / 2),
					plate.getY() - Math.abs(COPCrashBurst.getSize(getWidth(), H)));
			burst.setTimestamp(plate.getTimestamp());
			moving.add(burst);
		}
		for (GameObject m : moving.toArray(new GameObject[0])) {
			if (m instanceof COPPlateObject) {
				COPPlateObject plate = (COPPlateObject) m;
				double plateTime = time - plate.getTimestamp();
				if (plate.getMotionType() == COPPlateObject.PlateMotionType.REGULAR)
					plate.setY((int) (0.5 * GRAVITY * Math.pow(plateTime, 2.0) + plateTime * V_i));
				else if (plate.getMotionType() == COPPlateObject.PlateMotionType.BAZOOKA)
					plate.setY((int) (0.5 * GRAVITY * Math.pow(plateTime, 2.0) + plateTime * V_i * BAZOOKA_V_RATIO));
				if (!plate.isVisible()) {
					PLATES_FACTORY.collect(plate);
					moving.remove(m);
				} else if (plate.getY() > getHeight() - plate.getHeight()) {
					plate.setVisible(false);
					constant.add(new COPCrashBurst(
							plate.getX() - Math.abs((COPCrashBurst.getSize(getWidth(), W) - plate.getWidth()) / 2),
							plate.getY() - Math.abs(COPCrashBurst.getSize(getWidth(), H) - plate.getHeight())));
					totalCrashed++;
					continuouslyCrashed++;
				} else {
					CollectState state = hasCollect(plate);
					if (state != CollectState.FALSE) {
						if (plate.getMotionType() == COPPlateObject.PlateMotionType.REGULAR) {
							continuouslyCrashed = 0;
							plate.setVisible(false);
							if (state == CollectState.LEFT) {
								control.add(1, new PlateSpirit(this, plate, true));
								collectedLeft.add(plate.getColor());
							} else {
								control.add(1, new PlateSpirit(this, plate, false));
								collectedRight.add(plate.getColor());
							}
						} else if (plate.getMotionType() == COPPlateObject.PlateMotionType.BAZOOKA) {
							continuouslyCrashed = 0;
							plate.setVisible(false);
							bazookaWining(state == CollectState.LEFT);
						}

					}
				}
			} else if (m instanceof COPCrashBurst) {
				COPCrashBurst burst = (COPCrashBurst) m;
				double burstTime = time - burst.getTimestamp();
				burst.setY((int) (0.5 * GRAVITY * Math.pow(burstTime, 2.0) + burstTime * V_i * BAZOOKA_V_RATIO));
				if (!burst.isVisible()) {
					moving.remove(m);
				} else if (burst.getY() > getHeight()) {
					burst.isVisible();
				}
			}
		}
		for (GameObject c : constant.toArray(new GameObject[0])) {
			if (c instanceof COPCrashBurst) {
				COPCrashBurst burst = (COPCrashBurst) c;
				if (!burst.isVisible())
					constant.remove(c);
				else if (burst.currentFrame() > burst.getNumOfFrame() * 2) {
					burst.setVisible(false);
				}
			}
		}
		for (GameObject c : control.toArray(new GameObject[0])) {
			if (c instanceof PlateSpirit) {
				PlateSpirit spirit = (PlateSpirit) c;
				if (!spirit.isVisible()) {
					control.remove(c);
				}
			} else if (c instanceof COPCrashBurst) {
				COPCrashBurst burst = (COPCrashBurst) c;
				if (!burst.isVisible())
					constant.remove(c);
				else if (burst.currentFrame() > burst.getNumOfFrame() * 1.5) {
					burst.setVisible(false);
				}
			}
		}
		// check winning on each side
		for (boolean side : new boolean[] { true, false })
			checkWinning(side);
		if (continuouslyCrashed >= CONT_CRASHED_LIMIT) {
			decScore();
			continuouslyCrashed -= CONT_CRASHED_LIMIT;
		}
		if (totalCrashed >= TOTAL_CRASHED_LIMIT)
			timeout = true;
		time += T_g;
		scene++;
		
		if (GamePanel.gameOver) {
			GameOverFrame.getInstance().showFrame();
			GamePanel.closeGame();
			return false;
		}

		return true;
	}

	private enum CollectState {
		FALSE, LEFT, RIGHT
	}

	private static class Point {
		double x, y;

		Point(double x, double y) {
			this.x = x;
			this.y = y;
		}

		boolean in(Region region) {
			return between(this.x, region.x1, region.x2) && between(this.y, region.y1, region.y3);
		}
	}

	private static class Region {
		double x1, x2, y1, y3;

		Region(double x1, double x2, double y1, double y3) {
			this.x1 = x1;
			this.x2 = x2;
			this.y1 = y1;
			this.y3 = y3;
		}
	}

	private static boolean between(double a, double x1, double x2) {
		return a > Math.min(x1, x2) && a < Math.max(x1, x2);
	}

	private CollectState hasCollect(COPPlateObject plate) {

		COPClownObject clown = (COPClownObject) control.get(0);
		final Point[] PLATE_POINTS = { new Point(plate.getX() + plate.cons(A), plate.getY() + plate.cons(C)),
				new Point(plate.getX() + plate.cons(B), plate.getY() + plate.cons(C)),
				new Point(plate.getX() + plate.cons(A), plate.getY() + plate.cons(D)),
				new Point(plate.getX() + plate.cons(B), plate.getY() + plate.cons(D)) };
		final Region LEFT = new Region(clown.getX() + clown.cons(A), clown.getX() + clown.cons(B),
				clown.getY() + clown.cons(E) - collectedLeftHeight, clown.getY() + clown.cons(F) - collectedLeftHeight);
		final Region RIGHT = new Region(clown.getX() + clown.cons(C), clown.getX() + clown.cons(D),
				clown.getY() + clown.cons(E) - collectedRightHeight,
				clown.getY() + clown.cons(F) - collectedRightHeight);
		for (Point point : PLATE_POINTS) {
			if (point.in(LEFT))
				return CollectState.LEFT;
			else if (point.in(RIGHT))
				return CollectState.RIGHT;
		}
		return CollectState.FALSE;
	}

	private ArrayList<PlateColor> collectedLeft = new ArrayList<>();
	private ArrayList<PlateColor> collectedRight = new ArrayList<>();

	private boolean hasWonOn(ArrayList<PlateColor> collected) {
		boolean hasWon = false;
		if (collected.size() >= 3) {
			PlateColor lastColor = collected.get(collected.size() - 1);
			hasWon = lastColor == collected.get(collected.size() - 2)
					&& lastColor == collected.get(collected.size() - 3);
		}
		return hasWon;
	}

	private void checkWinning(boolean left) {
		ArrayList<PlateColor> collected = left ? collectedLeft : collectedRight;
		if (hasWonOn(collected)) {
			incScore();
			for (int i = 0; i < 3; i++)
				collected.remove(collected.size() - 1);
			int i = 0;
			for (GameObject c : control) {
				if (c instanceof PlateSpirit) {
					PlateSpirit spirit = (PlateSpirit) c;
					if (spirit.isLeft() == left && spirit.isVisible()) {
						spirit.setVisible(false);
						i++;
						if (left)
							collectedLeftHeight -= spirit.depth;
						else
							collectedRightHeight -= spirit.depth;
					}
					if (i == 3) {
						control.add(new COPCrashBurst(
								spirit.getX()
										- Math.abs((COPCrashBurst.getSize(getWidth(), W) - spirit.getWidth()) / 2),
								spirit.getY() - Math.abs(COPCrashBurst.getSize(getWidth(), H) - spirit.getHeight())));
						break;
					}
				}
			}
		}
	}

	private void bazookaWining(boolean left) {
		ArrayList<PlateColor> collected = left ? collectedLeft : collectedRight;
		modScore(collected.size());
		int j = collected.size();
		for (int i = 0; i < collected.size(); i++)
			collected.remove(0);
		for (GameObject c : control) {
			if (c instanceof PlateSpirit) {
				PlateSpirit spirit = (PlateSpirit) c;
				if (spirit.isLeft() == left && spirit.isVisible()) {
					spirit.setVisible(false);
					j--;
					if (left)
						collectedLeftHeight -= spirit.depth;
					else
						collectedRightHeight -= spirit.depth;
				}
				if (j == 0) {
					control.add(new COPCrashBurst(
							spirit.getX() - Math.abs((COPCrashBurst.getSize(getWidth(), W) - spirit.getWidth()) / 2),
							spirit.getY() - Math.abs(COPCrashBurst.getSize(getWidth(), H) - spirit.getHeight())));
					break;
				}
			}
		}
	}

	double collectedLeftHeight = 0, collectedRightHeight = 0;

	private class PlateSpirit implements GameObject {
		int x, y, width, height, leftBound, rightBound;
		boolean visible, left;
		double depth;
		PlateColor color;
		BufferedImage[] spiritImage;

		PlateSpirit(COPGameLoop gameLoop, COPPlateObject plate, boolean left) {
			final COPClownObject CLOWN = (COPClownObject) control.get(0);
			spiritImage = plate.getSpriteImages();
			color = plate.getColor();
			width = plate.getWidth();
			height = plate.getHeight();
			depth = plate.cons(E);
			this.visible = true;
			this.left = left;
			double relativePlateXPosition;
			double relativePlateYPosition;
			if (left) {
				collectedLeftHeight += plate.cons(E);
				relativePlateXPosition = (CLOWN.cons(B) - CLOWN.cons(A)) / 2.0 + CLOWN.cons(A) - plate.cons(W) / 2.0;
				relativePlateYPosition = CLOWN.cons(F) - collectedLeftHeight;

			} else {
				collectedRightHeight += plate.cons(E);
				relativePlateXPosition = (CLOWN.cons(D) - CLOWN.cons(C)) / 2.0 + CLOWN.cons(C) - plate.cons(W) / 2.0;
				relativePlateYPosition = CLOWN.cons(F) - collectedRightHeight;
			}
			x = (int) (CLOWN.getX() + relativePlateXPosition);
			y = (int) (CLOWN.getY() + relativePlateYPosition);
			leftBound = (int) relativePlateXPosition;
			rightBound = (int) (gameLoop.getWidth() - CLOWN.cons(W) + relativePlateXPosition);
		}

		public int getX() {
			return x;
		}

		public void setX(int x) {
			if (x > leftBound && x < rightBound) {
				this.x = x;
			} else if (x <= leftBound) {
				this.x = leftBound;
			} else if (x >= rightBound) {
				this.x = rightBound;
			}
		}

		public int getY() {
			return y;
		}

		public void setY(int y) {
		}

		@SuppressWarnings("unused")
		public PlateColor getColor() {
			return color;
		}

		public int getWidth() {
			return width;
		}

		public int getHeight() {
			return height;
		}

		public boolean isVisible() {
			return visible;
		}

		public boolean isLeft() {
			return left;
		}

		public void setVisible(boolean visible) {
			this.visible = visible;
		}

		public BufferedImage[] getSpriteImages() {
			return spiritImage;
		}
	}

}
