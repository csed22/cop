package eg.edu.alexu.csd.oop.game.controller.worldConstructor;

import eg.edu.alexu.csd.oop.game.controller.factory.PlatesFactory;
import eg.edu.alexu.csd.oop.game.model.object.COPClownObject;
import eg.edu.alexu.csd.oop.game.model.world.AbstractWorldModel;
import eg.edu.alexu.csd.oop.game.model.world.COPWorldModel;

import static eg.edu.alexu.csd.oop.game.model.object.SizeConstant.H;
import static eg.edu.alexu.csd.oop.game.model.object.SizeConstant.W;

/**
 * @dPatten Factory
 */
public class COPWorldConstructor extends AbstractWorldConstructor {

	public final Level LEVEL;
	public final PlatesFactory PLATES_FACTORY;

	// do nothing in the constructor, unless you want more info to be passed in
	public COPWorldConstructor(AbstractWorldModel world) {
		super(world);
		this.LEVEL = ((COPWorldModel) world).LEVEL;
		this.PLATES_FACTORY = ((COPWorldModel) world).PLATES_FACTORY;
	}

	public void buildWorldObjects() {
		PLATES_FACTORY.setLevel(Level.EASY);
		PLATES_FACTORY.setWorldWidth(getWidth());
		PLATES_FACTORY.fill();

		control.add(new COPClownObject((getWidth() - COPClownObject.getSize(width, W)) / 2,
				(int) (getHeight() - COPClownObject.getSize(width, H) - getHeight() * 0.025)));
			// pushing the clown up a bit
	}
}
