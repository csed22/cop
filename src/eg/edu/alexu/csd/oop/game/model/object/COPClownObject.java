package eg.edu.alexu.csd.oop.game.model.object;

import java.awt.image.BufferedImage;

import eg.edu.alexu.csd.oop.game.model.Turnable;

import static eg.edu.alexu.csd.oop.game.model.object.SizeConstant.*;

public class COPClownObject extends AbstractImageObject implements Turnable {

	public BufferedImage[] dirImagesL = new BufferedImage[NUM_OF_FRAMES];
	public BufferedImage[] dirImagesR = new BufferedImage[NUM_OF_FRAMES];

	public COPClownObject(int x, int y) {
		super(x, y, 129);
		ImagesVisitor.ImagesManger.adjust(this);
		objectImages = dirImagesR;
		this.visible = true;
	}

	/** all clown images native resolution 480 x 543 */
	public static int aspectRatio(int width) {
		return width * 543 / 480;
	}

	public void setX(int x) {
		if (x > this.x)
			turnRight();
		else if (x < this.x)
			turnLeft();
		this.x = x;
	}

	public void setY(int y) {
	}

	public void turnLeft() {
		objectImages = dirImagesL;
	}

	public void turnRight() {
		objectImages = dirImagesR;
	}

	/*
	 * this methods represent a storage for the class constants, instead of hard
	 * coding them. these constants are used to locate important regions on the
	 * object used in the game, or even to store the object's major size ratios
	 * instead of hard-coding them.
	 */
	public static double getRatio(SizeConstant constant) {
		switch (constant) {
		case WIDTH_TO_WORLD_WIDTH:
			return 2.0 / 9.0;
		case HEIGHT_TO_WIDTH:
			return 543.0 / 480.0;
		case A:
			return 30.0 / 1000;
		case B:
			return 260.0 / 1000;
		case E:
			return 465.0 / 1130.0;
		case F:
			return 520.0 / 1130.0;
		default:
			return 1.0;
		}
	}

	public static int getSize(int worldWidth, SizeConstant constant) {
		switch (constant) {
		case W:
			return (int) (worldWidth * getRatio(WIDTH_TO_WORLD_WIDTH));
		case H:
			return (int) (getSize(worldWidth, W) * getRatio(HEIGHT_TO_WIDTH));
		default:
			return 0;
		}
	}

	public double cons(SizeConstant constant) {
		switch (constant) {
		case W:
			return getWidth();
		case H:
			return getHeight();
		case A:
			return getWidth() * getRatio(A);
		case B:
			return getWidth() * getRatio(B);
		case C:
			return getWidth() - cons(B);
		case D:
			return getWidth() - cons(A);
		case E:
			return getHeight() * getRatio(E);
		case F:
			return getHeight() * getRatio(F);
		default:
			return 0.0;
		}
	}

}
