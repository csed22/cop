package eg.edu.alexu.csd.oop.game.model.world;

import eg.edu.alexu.csd.oop.game.controller.factory.PlatesFactory;
import eg.edu.alexu.csd.oop.game.controller.gameLoop.COPGameLoop;
import eg.edu.alexu.csd.oop.game.controller.gameLoop.ScoreModificationListener;
import eg.edu.alexu.csd.oop.game.controller.worldConstructor.COPWorldConstructor;
import eg.edu.alexu.csd.oop.game.controller.worldConstructor.Level;

/**
 * @dPattern Strategy
 */
public class COPWorldModel extends AbstractWorldModel {

	public final Level LEVEL;
	public final PlatesFactory PLATES_FACTORY = new PlatesFactory();
	public final COPWorldConstructor worldConstructor;
	public final COPGameLoop gameLoop;

	public COPWorldModel(int screenWidth, int screenHeight, Level level) {
		super(screenWidth, screenHeight);
		LEVEL = level;
		worldConstructor = new COPWorldConstructor(this);
		worldConstructor.buildWorldObjects();
		gameLoop = new COPGameLoop(this);
		gameLoop.setScoreListener(new ScoreModificationListener() {
			public void scoreModificationHappened(int scoreModification) {
				modifyScore(scoreModification);
			}
		});
	}

	public boolean refresh() {
		return gameLoop.updateAndCheck();
	}

}
