package eg.edu.alexu.csd.oop.game.model.object;

import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import eg.edu.alexu.csd.oop.game.COP;
import eg.edu.alexu.csd.oop.game.controller.displayPlates.PlateColor;
import eg.edu.alexu.csd.oop.game.controller.displayPlates.PlateShape;
import eg.edu.alexu.csd.oop.game.model.Quality;
import eg.edu.alexu.csd.oop.game.model.QualityState;

import static eg.edu.alexu.csd.oop.game.model.object.SizeConstant.*;
import static eg.edu.alexu.csd.oop.game.model.object.SizeConstant.E;

public class COPPlateObject extends AbstractImageObject {

	public final PlateShape shape;
	public final PlateColor color;
	private double timestamp;
	private PlateMotionType motionType;

	public enum PlateMotionType {
		REGULAR, ROCKET_0, ROCKET_1, BAZOOKA
	}

	public COPPlateObject(int x, int y, PlateShape shape, PlateColor color) {
		super(x, y, 1);
		this.shape = shape;
		this.color = color;
		importObjectImages(shape.getName() + "-" + color.getName());
		resizeObjectImagesToFit();
		this.motionType = PlateMotionType.REGULAR;
		this.visible = true;
	}

	public void setMotionType(PlateMotionType motionType) {
		this.motionType = motionType;
	}

	public PlateMotionType getMotionType() {
		return motionType;
	}

	public void setTimestamp(double timestamp) {
		this.timestamp = timestamp;
	}

	public double getTimestamp() {
		return timestamp;
	}

	/** all the plate images native resolution is 96 x 21 */
	public int aspectRatio(int width) {
		return (int) (width * getRatio(shape, HEIGHT_TO_WIDTH));
	}

	protected void importObjectImages(String path) {
		try {
			String plate = "resources/plates/" + path + ".png";
			objectImages[0] = ImageIO.read(new File(plate));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	protected void resizeObjectImagesToFit() {
		int width = (int) (ImagesVisitor.SUPPOSED_SCREEN_WIDTH * getRatio(shape, WIDTH_TO_WORLD_WIDTH) / COP.RATIO_TO_1080);
		Quality picQuality = QualityState.getInstance().getQuality();
		if(picQuality != Quality.HIGH)
			objectImages[0] = resizeImage(objectImages[0], width/picQuality.getNum(), aspectRatio(width/picQuality.getNum()));
		objectImages[0] = resizeImage(objectImages[0], width, aspectRatio(width));
	}

	@Override
	public AbstractImageObject clone() {
		// TODO Auto-generated method stub
		return null;
	}

	public PlateColor getColor() {
		return color;
	}

	private static double getRatio(PlateShape shape, SizeConstant constant) {
		switch (shape) {
		case PLATE:
			switch (constant) {
			case WIDTH_TO_WORLD_WIDTH:
				return 1.0 / 18.0;
			case HEIGHT_TO_WIDTH:
				return 21.0 / 96.0;
			case A:
				return 150.0 / 1000.0;
			case C:
				return 80.0 / 220.0;
			case D:
				return 0.0 / 220.0;
			case E:
				return 90.0 / 100.0;
			default:
				return 1.0;
			}
		case BOWL:
			switch (constant) {
			case WIDTH_TO_WORLD_WIDTH:
				return 1.0 / 26.0;
			case HEIGHT_TO_WIDTH:
				return 93.0 / 146.0;
			case A:
				return 0.0 / 1000.0;
			case C:
				return 0.0 / 637.0;
			case D:
				return 1 - 525.0 / 637.0;
			case E:
				return 90.0 / 100.0;
			default:
				return 1.0;
			}
		case DISH:
			switch (constant) {
			case WIDTH_TO_WORLD_WIDTH:
				return 1.0 / 18.0;
			case HEIGHT_TO_WIDTH:
				return 16.0 / 142.0;
			case A:
				return 60.0 / 1000.0;
			case C:
				return 0.0 / 113.0;
			case D:
				return 0.0 / 113.0;
			case E:
				return 90.0 / 100.0;
			default:
				return 1.0;
			}
		default:
			return 1.0;
		}
	}

	public static int getSize(int worldWidth, PlateShape shape, SizeConstant constant) {
		switch (constant) {
		case W:
			return (int) (worldWidth * getRatio(shape, WIDTH_TO_WORLD_WIDTH));
		case H:
			return (int) (getSize(worldWidth, shape, W) * getRatio(shape, HEIGHT_TO_WIDTH));
		default:
			return 0;
		}
	}

	public double cons(SizeConstant constant) {
		switch (constant) {
		case W:
			return getWidth();
		case H:
			return getHeight();
		case A:
			return getWidth() * getRatio(shape, A);
		case B:
			return getWidth() - cons(A);
		case C:
			return getHeight() * getRatio(shape, C);
		case D:
			return getHeight() - getRatio(shape, D) * getHeight();
		case E:
			return (cons(D) - cons(C)) * getRatio(shape, E);
		default:
			return 0.0;
		}
	}
}
