package eg.edu.alexu.csd.oop.game.model;

import eg.edu.alexu.csd.oop.game.view.observer.Observer;

public class QualityState implements Observer {

	private static QualityState single_instance = null;

	private QualityState() {
		this.quality = Quality.HIGH;
	}

	public static QualityState getInstance() {
		if (single_instance == null)
			single_instance = new QualityState();

		return single_instance;
	}

	private Quality quality;

	// Different states expected

	private void highGraphics() {
		this.quality = Quality.HIGH;
	}

	private void mediumGraphic() {
		this.quality = Quality.MEDIUM;
	}

	private void lowGraphic() {
		this.quality = Quality.LOW;
	}

	public Quality getQuality() {
		return quality;
	}

	public void update(int value) {
		switch (value) {
		case 0:
			lowGraphic();
			break;
		case 1:
			mediumGraphic();
			break;
		case 2:
			highGraphics();
			break;
		default:
			break;
		}

	}

}
