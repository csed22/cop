package eg.edu.alexu.csd.oop.game.model.world;

import java.util.LinkedList;
import java.util.List;

import eg.edu.alexu.csd.oop.game.GameObject;
import eg.edu.alexu.csd.oop.game.World;
import eg.edu.alexu.csd.oop.game.view.ScoreUpdatingListener;

/**
 * @note contains all common functionality from example objects.
 */
public abstract class AbstractWorldModel implements World {

	public int MAX_TIME = 2 * 60 * 1000;
	public long startTime = System.currentTimeMillis();

	protected int score = 0;
	// public final double T_g = 10 * 0.001; //game speed in seconds
	// public final double D_c = 10; //control speed

	private final int width;
	private final int height;

	public final List<GameObject> constant = new LinkedList<GameObject>();
	public final List<GameObject> moving = new LinkedList<GameObject>();
	public final List<GameObject> control = new LinkedList<GameObject>();

	public AbstractWorldModel(int screenWidth, int screenHeight) {
		width = screenWidth;
		height = screenHeight;
	}

	public List<GameObject> getConstantObjects() {
		return constant;
	}

	public List<GameObject> getMovableObjects() {
		return moving;
	}

	public List<GameObject> getControlableObjects() {
		return control;
	}

	// mine
	private ScoreUpdatingListener scoreListener;

	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}

	public int getSpeed() {
		return /* (int) (T_g * 1000) */ 10;
	}

	public int getControlSpeed() {
		return /* (int) D_c */ 15;
	}

	// mine
	public void modifyScore(int modification) {
		score = Math.max(0, score + modification);
		if (scoreListener != null) {
			scoreListener.scoreUpdated(getScore());
		}
	}

	// mine
	public void addScoreUpdatingListener(ScoreUpdatingListener scoreListener) {
		this.scoreListener = scoreListener;
	}

	public int getScore() {
		return score;
	}

	public String getStatus() {
		return "Score=" + getScore() + "   |   Time="
				+ Math.max(0, (MAX_TIME - (System.currentTimeMillis() - startTime)) / 1000);
	}

}
