package eg.edu.alexu.csd.oop.game.model.object;

import static eg.edu.alexu.csd.oop.game.model.object.SizeConstant.HEIGHT_TO_WIDTH;
import static eg.edu.alexu.csd.oop.game.model.object.SizeConstant.W;
import static eg.edu.alexu.csd.oop.game.model.object.SizeConstant.WIDTH_TO_WORLD_WIDTH;

/**
 * @dPattern Flyweight (tomorrow will be implemented)
 */
public class COPCrashBurst extends AbstractImageObject {
	
	int currentFrame = 0;
	double timestamp = 0;

	public COPCrashBurst(int x, int y) {
		super(x, y, 9);
		ImagesVisitor.ImagesManger.adjust(this);
		this.visible = true;
	}

	public void setTimestamp(double timestamp) {
		this.timestamp = timestamp;
	}

	public double getTimestamp() {
		return timestamp;
	}

	public static double getRatio(SizeConstant constant) {
		switch (constant) {
		case WIDTH_TO_WORLD_WIDTH:
			return 1.0 / 19.2;		// modified to make sure that it is HD : old value (1.0/12.0)
		case HEIGHT_TO_WIDTH:
			return 1.0 / 1.0;
		default:
			return 1.0;
		}
	}

	public static int getSize(int worldWidth, SizeConstant constant) {
		switch (constant) {
		case W:
			return (int) (worldWidth * getRatio(WIDTH_TO_WORLD_WIDTH));
		case H:
			return (int) (getSize(worldWidth, W) * getRatio(HEIGHT_TO_WIDTH));
		default:
			return 0;
		}
	}

	public double cons(SizeConstant constant) {
		switch (constant) {
		case W:
			return getWidth();
		case H:
			return getHeight();
		default:
			return 0.0;
		}
	}

	public int currentFrame() {
		return currentFrame++;
	}

	public int getNumOfFrame() {
		return NUM_OF_FRAMES;
	}
}
