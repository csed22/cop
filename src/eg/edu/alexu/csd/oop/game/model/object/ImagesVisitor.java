package eg.edu.alexu.csd.oop.game.model.object;

import static eg.edu.alexu.csd.oop.game.model.object.SizeConstant.WIDTH_TO_WORLD_WIDTH;

import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;

import javax.imageio.ImageIO;

import eg.edu.alexu.csd.oop.game.model.Quality;
import eg.edu.alexu.csd.oop.game.model.QualityState;

import static eg.edu.alexu.csd.oop.game.COP.RATIO_TO_1080;

/**
 * @dPattern Flyweight, Visitor
 */
public interface ImagesVisitor {

	// Created to automatically use the right
	// code based on the Object sent
	// Method Overloading

	void adjust(COPCrashBurst crash);

	void adjust(COPClownObject clown);

	ImagesManipulator ImagesManger = new ImagesManipulator();

	// this declaration are mine
	double SUPPOSED_SCREEN_WIDTH = 1920;

	class ImagesManipulator implements ImagesVisitor {

		// A map of all images to be loaded and shared between all objects
		private HashMap<Class<? extends AbstractImageObject>, BufferedImage[][]> imagesCollection;

		QualityState picQuality = QualityState.getInstance();

		private int previousWidth = 0;

		public void readThemAll() {

			int currentWidth = (int) Math.ceil(SUPPOSED_SCREEN_WIDTH / RATIO_TO_1080);
			
			if (previousWidth == currentWidth)
				return;

			previousWidth = currentWidth;

			BufferedImage[][] clownImages = new BufferedImage[6][129];

			int clownExpectedWidth = (int) Math
					.ceil(SUPPOSED_SCREEN_WIDTH * COPClownObject.getRatio(WIDTH_TO_WORLD_WIDTH));

			int clownActualWidth = (int) Math
					.ceil(SUPPOSED_SCREEN_WIDTH * COPClownObject.getRatio(WIDTH_TO_WORLD_WIDTH) / RATIO_TO_1080);

			try {
				for (int i = 0; i < 129; i++) {
					String sL = "resources/toLeft/clownL" + i + ".png";
					String sR = "resources/toRight/clownR" + i + ".png";
					clownImages[4][i] = ImageIO.read(new File(sL));
					clownImages[5][i] = ImageIO.read(new File(sR));

					if (clownActualWidth != clownExpectedWidth) {
						clownImages[4][i] = resizeImage(clownImages[4][i], clownActualWidth,
								COPClownObject.aspectRatio(clownActualWidth));
						clownImages[5][i] = resizeImage(clownImages[5][i], clownActualWidth,
								COPClownObject.aspectRatio(clownActualWidth));
					}

				}
			} catch (IOException e) {
				e.printStackTrace();
			}

			clownImages[2] = pixelationFilter(COPClownObject.class, clownImages[4], Quality.MEDIUM);
			clownImages[0] = pixelationFilter(COPClownObject.class, clownImages[4], Quality.LOW);
			clownImages[3] = pixelationFilter(COPClownObject.class, clownImages[5], Quality.MEDIUM);
			clownImages[1] = pixelationFilter(COPClownObject.class, clownImages[5], Quality.LOW);

			imagesCollection.put(COPClownObject.class, clownImages);
			
			BufferedImage[][] brustImages = new BufferedImage[3][9];

			int brustExpectedWidth = (int) Math
					.ceil(SUPPOSED_SCREEN_WIDTH * COPCrashBurst.getRatio(WIDTH_TO_WORLD_WIDTH));

			int brustActualWidth = (int) Math
					.ceil(SUPPOSED_SCREEN_WIDTH * COPCrashBurst.getRatio(WIDTH_TO_WORLD_WIDTH) / RATIO_TO_1080);

			try {
				for (int i = 0; i < 9; i++) {
					String s = "resources/crash/crash" + i + ".png";
					brustImages[2][i] = ImageIO.read(new File(s));

					if (brustActualWidth != brustExpectedWidth)
						brustImages[2][i] = resizeImage(brustImages[2][i], brustActualWidth,
								brustActualWidth);
				}
			} catch (IOException e) {
				e.printStackTrace();
			}

			brustImages[1] = pixelationFilter(COPCrashBurst.class, brustImages[2], Quality.MEDIUM);
			brustImages[0] = pixelationFilter(COPCrashBurst.class, brustImages[2], Quality.LOW);

			imagesCollection.put(COPCrashBurst.class, brustImages);

		}

		private ImagesManipulator() {
			imagesCollection = new HashMap<Class<? extends AbstractImageObject>, BufferedImage[][]>();
		}

		public void adjust(COPCrashBurst crash) {
			switch (picQuality.getQuality()) {
			case LOW:
				crash.objectImages = imagesCollection.get(crash.getClass())[0];
				break;
			case MEDIUM:
				crash.objectImages = imagesCollection.get(crash.getClass())[1];
				break;
			case HIGH:
				crash.objectImages = imagesCollection.get(crash.getClass())[2];
				break;
			default:
				break;
			}
		}

		public void adjust(COPClownObject clown) {
			switch (picQuality.getQuality()) {
			case LOW:
				clown.dirImagesL = imagesCollection.get(clown.getClass())[0];
				clown.dirImagesR = imagesCollection.get(clown.getClass())[1];
				break;
			case MEDIUM:
				clown.dirImagesL = imagesCollection.get(clown.getClass())[2];
				clown.dirImagesR = imagesCollection.get(clown.getClass())[3];
				break;
			case HIGH:
				clown.dirImagesL = imagesCollection.get(clown.getClass())[4];
				clown.dirImagesR = imagesCollection.get(clown.getClass())[5];
				break;
			default:
				break;
			}
		}

		private BufferedImage[] pixelationFilter(Class<? extends AbstractImageObject> ownerClass,
				BufferedImage[] images, Quality quality) {
			final int WIDTH = images[0].getWidth();
			final int HEIGHT;

			if (ownerClass.equals(COPClownObject.class))
				HEIGHT = COPClownObject.aspectRatio(WIDTH);
			else
				HEIGHT = WIDTH;

			BufferedImage[] pixeledImages = new BufferedImage[images.length];

			for (int i = 0; i < images.length; i++) {
				pixeledImages[i] = resizeImage(images[i], WIDTH / quality.getNum(), HEIGHT / quality.getNum());
				pixeledImages[i] = resizeImage(pixeledImages[i], WIDTH, HEIGHT);
			}

			return pixeledImages;
		}

		// https://stackoverflow.com/questions/9417356/bufferedimage-resize
		private BufferedImage resizeImage(BufferedImage img, int newW, int newH) {
			Image tmp = img.getScaledInstance(newW, newH, Image.SCALE_SMOOTH);
			BufferedImage dimg = new BufferedImage(newW, newH, BufferedImage.TYPE_INT_ARGB);

			Graphics2D g2d = dimg.createGraphics();
			g2d.drawImage(tmp, 0, 0, null);
			g2d.dispose();

			return dimg;
		}

	}

}
